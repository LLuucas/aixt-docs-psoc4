// This C code was automatically generated by Aixt Project
// Device = x64
// Board = x64-based board
// Backend = pc

#include <stdbool.h>

#include "/home/aixt-project/ports/PC/api/builtin.c"


int main() {
	for(int i=0; i<5; i++) {
		print("Hello ");
		println("world!");
	}
	return 0;
}