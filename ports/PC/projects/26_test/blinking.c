// This C code was automatically generated by Aixt Project
// Device = x64
// Board = x64-based board
// Backend = pc



#include <stdbool.h>
#include "/home/aixt-project/ports/PC/api/builtin.c"
#include "/home/aixt-project/ports/PC/api/machine/pin.c"
#include "/home/aixt-project/ports/PC/api/time/sleep_ms.c"


int main() {
	while(true) {
		pin_high(x);
		sleep_ms(500);
		pin_low(x);
		sleep_ms(500);
	}
	return 0;
}